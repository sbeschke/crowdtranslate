from uuid import UUID

from pydantic import BaseModel


class TranslationRequest(BaseModel):
    request_id: UUID
    source_language: str
    target_language: str
    source_text: str


class TranslationResponse(BaseModel):
    request_id: UUID
    target_text: str
