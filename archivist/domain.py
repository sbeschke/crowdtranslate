from dataclasses import dataclass
from typing import Dict, List
from uuid import UUID


# Aggregate: Corpus

@dataclass
class Corpus:
    id: UUID
    name: str
    sections: List['CorpusSection']


@dataclass
class CorpusSection:
    name: str


# Aggregate: Sentence

@dataclass
class Sentence:
    id: UUID
    corpus_id: UUID
    section_name: str
    seq_index: int

    # language code -> string representation
    text: Dict[str, str]


# Aggregate: Rating

@dataclass
class Rating:
    id: UUID
    sentence_id: UUID

    rated_translation: 'Translation'
    # 1 to 5 stars
    star_rating: int


# Value Classes

@dataclass
class Translation:
    source_language: str
    source_text: str
    target_language: str
    target_text: str
