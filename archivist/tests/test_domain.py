from .. import domain

def test_rate_suggested_sentence(service):
    sentence = service.suggest_sentence_for_rating(source_language='en', target_language='de')
    assert(isinstance(sentence, domain.Sentence))
    translation = sentence.translation('en', 'de')
    assert(isinstance(translation, domain.Translation))
    service.submit_rating(sentence_id=sentence.id, rated_translation=translation, star_rating=4)
