import os
from uuid import UUID

from fastapi import Depends, FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address
from slowapi.errors import RateLimitExceeded

from . import api
from .application import Application
from .application.gateway_service import GatewayService


# Read config from env
GATEWAY_USE_RATE_LIMITING = os.getenv("GATEWAY_USE_RATE_LIMITING", "1")


app = FastAPI()
limiter = Limiter(key_func=get_remote_address, application_limits=["60/minute"], enabled=(GATEWAY_USE_RATE_LIMITING == "1"))
app.state.limiter = limiter
app.add_exception_handler(RateLimitExceeded, _rate_limit_exceeded_handler)

origins = [
    "http://localhost:3000",
    "http://localhost:8080",
    "http://feather.sbeschke.de",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event('startup')
async def startup():
    await Application.build()

async def get_service() -> GatewayService:
    return Application.get().gateway_service


@app.get('/status/')
def status(service: GatewayService = Depends(get_service)) -> api.AppStatus:
    return api.AppStatus(
        sourceLanguages=service.source_languages(),
        targetLanguages=service.target_languages()
    )


@app.post('/translate/')
@limiter.limit("1/20seconds")
async def translate(request: Request, translation_request: api.TranslationRequest, service: GatewayService = Depends(get_service)) -> api.TranslationStatus:
    request_id = await service.submit_translation_request(
            source_text=translation_request.sourceText,
            source_language=translation_request.sourceLanguage,
            target_language=translation_request.targetLanguage)
    status = service.translation_request_status(request_id)
    url = app.url_path_for('get_translation_status', request_id=request_id)
    return api.TranslationStatus(url=url, requestId=request_id, status=status.status_code.name)


@app.get('/translate/{request_id}/')
def get_translation_status(request_id: UUID, service: GatewayService = Depends(get_service)):
    status = service.translation_request_status(request_id)
    if status is None:
        raise HTTPException(status_code=404, detail=f'Request {request_id} not found')
    url = app.url_path_for('get_translation_status', request_id=request_id)
    return api.TranslationStatus(
        url=url,
        requestId=request_id,
        status=status.status_code.name,
        result=api.TranslationResult(targetText=status.response.target_text) if status.response else None)


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
