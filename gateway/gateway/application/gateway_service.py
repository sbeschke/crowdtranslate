from typing import List
from uuid import UUID

from gateway.infrastructure.translation_backends import TranslationBackend
from gateway.infrastructure.text_splitter import TextSplitter
from gateway import domain

class GatewayService:
    """Handles translation requests, sending them off to a backend and keeping
    track of their status."""
    def __init__(self,
                 translation_backend: TranslationBackend,
                 request_tracker: domain.RequestTracker,
                 text_splitter: TextSplitter):
        self.translation_backend = translation_backend
        self.request_tracker = request_tracker
        self.text_splitter = text_splitter

    async def submit_translation_request(self,
            source_text: str,
            source_language: str,
            target_language: str) -> domain.TranslationRequestStatus:
        chunks = self.text_splitter.split(source_text)
        chunk_request_ids = [self.request_tracker.queue_request() for _ in chunks]
        for chunk_request_id, chunk in zip(chunk_request_ids, chunks):
            request = domain.TranslationRequest(
                id=chunk_request_id,
                source_text=chunk,
                source_language=source_language,
                target_language=target_language
            )
            await self.translation_backend.submit_translation_request(request)
        request_id = self.request_tracker.group_requests(chunk_request_ids)
        return request_id

    def translation_request_status(self, request_id: UUID) -> domain.TranslationRequestStatus:
        return self.request_tracker.group_status(request_id)

    def source_languages(self) -> List[str]:
        return self.translation_backend.source_languages()

    def target_languages(self) -> List[str]:
        return self.translation_backend.target_languages()
