import logging

from fastapi import FastAPI

logger = logging.getLogger(__name__)

class Application:
    """Dependency root of the service."""

    _mode = 'production'
    _instance = None

    def __init__(self, gateway_service):
        self.gateway_service = gateway_service

    @classmethod
    def configure(cls, mode: str = 'production'):
        cls._mode = mode

    @classmethod
    async def build(cls):
        if cls._instance is not None:
            logger.error('Application has already been build!')
        if cls._mode == 'production':
            from ..bootstrap.production import bootstrap
            cls._instance = await bootstrap()
        if cls._mode == 'test':
            from ..bootstrap.test import bootstrap
            cls._instance = await bootstrap()

    @classmethod
    def get(cls):
        return cls._instance
