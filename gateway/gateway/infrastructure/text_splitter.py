import spacy

class TextSplitter:
    """Splits an incoming text into chunks.

    Attempts to split on sentence boundaries, then on token boundaries.
    """

    def __init__(self, max_characters: int):
        self.max_characters = max_characters
        self.nlp = spacy.load('xx_sent_ud_sm')

    def split(self, text: str) -> list[str]:
        doc = self.nlp(text)
        return list(self._generate_chunks_from_sents(doc.sents))

    def _generate_chunks_from_sents(self, sents):
        for sent in sents:
            if len(str(sent)) <= self.max_characters:
                yield str(sent)
            else:
                for chunk in self._generate_chunks_from_toks(sent):
                    yield chunk

    def _generate_chunks_from_toks(self, span):
        if len(span) == 0:
            return
        end = len(span)
        while len(str(span[:end])) > self.max_characters:
            end -= 1
        if end > 0:
            yield str(span[:end])
            for chunk in self._generate_chunks_from_toks(span[end:]):
                yield chunk
        else:
            for chunk in self._generate_chunks_from_token(span[0]):
                yield chunk
            for chunk in self._generate_chunks_from_toks(span[1:]):
                yield chunk
            
    def _generate_chunks_from_token(self, token):
        if len(token) == 0:
            return
        yield str(token)[:self.max_characters]
        for chunk in self._generate_chunks_from_token(str(token)[self.max_characters:]):
            yield chunk