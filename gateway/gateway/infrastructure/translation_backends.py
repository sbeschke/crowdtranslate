from abc import ABC, abstractmethod
import logging
import re
from typing import Callable, List, NoReturn
from uuid import UUID, uuid4

import aio_pika

from crowdtranslate_messaging import messaging

from gateway import domain


logger = logging.getLogger(__name__)


# As long as we only have one model, the list of available languages is defined here.
# Copied from: https://huggingface.co/facebook/m2m100_1.2B
m2m_languages_str = """Afrikaans (af), Amharic (am), Arabic (ar), Asturian (ast), Azerbaijani (az), Bashkir (ba), Belarusian (be), Bulgarian (bg), Bengali (bn), Breton (br), Bosnian (bs), Catalan; Valencian (ca), Cebuano (ceb), Czech (cs), Welsh (cy), Danish (da), German (de), Greeek (el), English (en), Spanish (es), Estonian (et), Persian (fa), Fulah (ff), Finnish (fi), French (fr), Western Frisian (fy), Irish (ga), Gaelic; Scottish Gaelic (gd), Galician (gl), Gujarati (gu), Hausa (ha), Hebrew (he), Hindi (hi), Croatian (hr), Haitian; Haitian Creole (ht), Hungarian (hu), Armenian (hy), Indonesian (id), Igbo (ig), Iloko (ilo), Icelandic (is), Italian (it), Japanese (ja), Javanese (jv), Georgian (ka), Kazakh (kk), Central Khmer (km), Kannada (kn), Korean (ko), Luxembourgish; Letzeburgesch (lb), Ganda (lg), Lingala (ln), Lao (lo), Lithuanian (lt), Latvian (lv), Malagasy (mg), Macedonian (mk), Malayalam (ml), Mongolian (mn), Marathi (mr), Malay (ms), Burmese (my), Nepali (ne), Dutch; Flemish (nl), Norwegian (no), Northern Sotho (ns), Occitan (post 1500) (oc), Oriya (or), Panjabi; Punjabi (pa), Polish (pl), Pushto; Pashto (ps), Portuguese (pt), Romanian; Moldavian; Moldovan (ro), Russian (ru), Sindhi (sd), Sinhala; Sinhalese (si), Slovak (sk), Slovenian (sl), Somali (so), Albanian (sq), Serbian (sr), Swati (ss), Sundanese (su), Swedish (sv), Swahili (sw), Tamil (ta), Thai (th), Tagalog (tl), Tswana (tn), Turkish (tr), Ukrainian (uk), Urdu (ur), Uzbek (uz), Vietnamese (vi), Wolof (wo), Xhosa (xh), Yiddish (yi), Yoruba (yo), Chinese (zh), Zulu (zu)"""
language_re = re.compile(r'\(([a-z]{2})\)')
M2M_LANGUAGES = language_re.findall(m2m_languages_str)


class TranslationBackend(ABC):
    @abstractmethod
    async def submit_translation_request(self,
                                         request: domain.TranslationRequest) -> UUID:
        pass

    @abstractmethod
    def source_languages(self) -> List[str]:
        pass

    @abstractmethod
    def target_languages(self) -> List[str]:
        pass


class DummyTranslationBackend(TranslationBackend):
    async def init(self):
        pass

    async def submit_translation_request(
            self,
            request: domain.TranslationRequest) -> UUID:
        return uuid4()

    def source_languages(self) -> List[str]:
        return M2M_LANGUAGES

    def target_languages(self) -> List[str]:
        return M2M_LANGUAGES


class PikaTranslationBackend(TranslationBackend):
    def __init__(self, queue_host: str = 'localhost'):
        self.queue_host = queue_host
        self.listeners = []

    async def init(self):
        connection = await aio_pika.connect_robust(
            f"amqp://guest:guest@{self.queue_host}/"
        )

        self.channel = await connection.channel()

        self.requests_exchange = await self.channel.declare_exchange(name='translation_requests', type='topic')
        self.responses_exchange = await self.channel.declare_exchange(name='translation_responses', type='fanout')

        self.queue = await self.channel.declare_queue('', auto_delete=True)
        await self.queue.bind(self.responses_exchange)
        await self.queue.consume(self.process_message)

    def add_listener(self, listener: Callable[[messaging.TranslationResponse], NoReturn]):
        self.listeners.append(listener)

    def source_languages(self) -> List[str]:
        return M2M_LANGUAGES

    def target_languages(self) -> List[str]:
        return M2M_LANGUAGES

    async def submit_translation_request(self,
                                         request: domain.TranslationRequest):
        # TODO iterate over chunks
        request = messaging.TranslationRequest(
            request_id=request.id,
            source_text=request.source_text,
            source_language=request.source_language,
            target_language=request.target_language
        )
        routing_key = f'translation.{request.source_language}.{request.target_language}'
        await self.requests_exchange.publish(
            aio_pika.Message(
                bytes(request.json(), 'utf-8'),
                content_type='application/json',
            ),
            routing_key,
        )

    async def process_message(self, message: aio_pika.IncomingMessage):
        async with message.process():
            response = messaging.TranslationResponse.parse_raw(message.body)
            logger.info(f'Received translation: {response.target_text}')
            for listener in self.listeners:
                listener(response)
