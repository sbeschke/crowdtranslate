from dataclasses import dataclass
from uuid import UUID


@dataclass
class TranslationRequest:
    id: UUID
    source_language: str
    target_language: str
    source_text: str
    source_text_chunks: list[str] | None = None
