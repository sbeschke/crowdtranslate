from dataclasses import dataclass
from enum import Enum
from functools import reduce
import logging
from typing import Iterable, Optional
from uuid import UUID, uuid4

from crowdtranslate_messaging import messaging


logger = logging.getLogger(__name__)


@dataclass
class ResponseData:
    target_text: str

    @classmethod
    def combine(cls,
                one: Optional['ResponseData'],
                other: Optional['ResponseData']
    ) -> Optional['ResponseData']:
        if one is None:
            return other
        if other is None:
            return one
        return ResponseData(
            target_text=one.target_text + ' ' + other.target_text
        )


class TranslationRequestStatusCode(Enum):
    QUEUED = 1
    COMPLETED = 2
    FAILED = 3

    @classmethod
    def combine(cls,
                one: 'TranslationRequestStatusCode', 
                other: 'TranslationRequestStatusCode'
    ) -> 'TranslationRequestStatusCode':
        if one == cls.FAILED or other == cls.FAILED:
            return cls.FAILED
        if one == cls.QUEUED or other == cls.QUEUED:
            return cls.QUEUED
        return cls.COMPLETED


@dataclass
class TranslationRequestStatus:
    request_id: UUID
    status_code: TranslationRequestStatusCode
    response: Optional[ResponseData] = None

    @classmethod
    def combine(self,
                one: 'TranslationRequestStatus',
                other: 'TranslationRequestStatus'
    ) -> 'TranslationRequestStatus':
        return TranslationRequestStatus(
            request_id=one.request_id,
            status_code = TranslationRequestStatusCode.combine(one.status_code, other.status_code),
            response = ResponseData.combine(one.response, other.response)
        )


@dataclass
class RequestGroup:
    request_id: UUID
    sub_request_ids: list[UUID]


class RequestTracker:
    """Keeps track of translation requests and their state."""
    def __init__(self):
        self.queued_requests = {}
        self.completed_requests = {}
        self.request_groups = {}

    def queue_request(self) -> UUID:
        request_id = uuid4()
        status = TranslationRequestStatus(
            request_id=request_id,
            status_code=TranslationRequestStatusCode.QUEUED)
        self.queued_requests[request_id] = status
        return request_id
    
    def group_requests(self, sub_request_ids: Iterable[UUID]) -> UUID:
        """Generates a UUID for a group of (already queued) requests so that they can be tracked
        as a unit.
        """
        request_id = uuid4()
        group = RequestGroup(
            request_id=request_id,
            sub_request_ids=list(sub_request_ids))
        self.request_groups[request_id] = group
        return request_id

    def receive_response(self, response: messaging.TranslationResponse):
        self.complete_request(response.request_id, ResponseData(response.target_text))

    def complete_request(self, request_id: UUID, response_data: ResponseData):
        try:
            del self.queued_requests[request_id]
        except KeyError:
            logger.warn(f'Received response for request {request_id} which is not queued')
        
        self.completed_requests[request_id] = TranslationRequestStatus(
            request_id=request_id,
            status_code=TranslationRequestStatusCode.COMPLETED,
            response=response_data
        )

    def fail_request(self, request_id: UUID):
        try:
            del self.queued_requests[request_id]
        except KeyError:
            logger.warn(f'Received failure notice for request {request_id} which is not queued')
        
        self.completed_requests[request_id] = TranslationRequestStatus(
            request_id=request_id,
            status_code=TranslationRequestStatusCode.FAILED
        )

    def request_status(self, request_id: UUID) -> Optional[TranslationRequestStatus]:
        status = self.completed_requests.get(request_id)
        if not status:
            status = self.queued_requests.get(request_id)
        return status


    def group_status(self, request_id: UUID) -> Optional[TranslationRequestStatus]:
        group = self.request_groups.get(request_id)
        if group is None:
            return None
        statuses = [self.request_status(sub_request_id) for sub_request_id in group.sub_request_ids]
        if any(s is None for s in statuses):
            return None
        
        combined_status = reduce(TranslationRequestStatus.combine, statuses)
        combined_status.request_id = request_id  # request_id not handled by combine
        return combined_status
