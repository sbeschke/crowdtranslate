from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel


class AppStatus(BaseModel):
    sourceLanguages: List[str]
    targetLanguages: List[str]


class TranslationRequest(BaseModel):
    sourceLanguage: str
    targetLanguage: str
    sourceText: str


class TranslationResult(BaseModel):
    targetText: str


class TranslationStatus(BaseModel):
    url: str
    requestId: UUID
    status: str
    result: Optional[TranslationResult] = None
