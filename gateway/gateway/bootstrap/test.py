import logging
import sys

from .. import domain
from ..application import Application
from ..infrastructure.text_splitter import TextSplitter
from ..infrastructure.translation_backends import DummyTranslationBackend
from ..application.gateway_service import GatewayService

async def bootstrap() -> Application:
    logging.getLogger().addHandler(logging.StreamHandler(sys.stderr))
    logging.getLogger().setLevel(logging.DEBUG)

    translation_backend = DummyTranslationBackend()
    await translation_backend.init()
    request_tracker = domain.RequestTracker()
    text_splitter = TextSplitter(512)
    service = GatewayService(
        translation_backend=translation_backend,
        request_tracker=request_tracker,
        text_splitter=text_splitter
    )

    return Application(gateway_service=service)
