import logging
import os
import sys

from fastapi import FastAPI

from ..application import Application
from ..application.gateway_service import GatewayService
from .. import domain
from ..infrastructure.text_splitter import TextSplitter
from ..infrastructure.translation_backends import PikaTranslationBackend

async def bootstrap() -> Application:
    logging.getLogger().addHandler(logging.StreamHandler(sys.stderr))
    logging.getLogger().setLevel(logging.DEBUG)

    queue_host = os.getenv('QUEUE_HOST', 'localhost')
    translation_backend = PikaTranslationBackend(queue_host=queue_host)
    await translation_backend.init()
    request_tracker = domain.RequestTracker()
    text_splitter = TextSplitter(512)
    service = GatewayService(
        translation_backend=translation_backend,
        request_tracker=request_tracker,
        text_splitter=text_splitter)
    translation_backend.add_listener(request_tracker.receive_response)

    return Application(gateway_service=service)
