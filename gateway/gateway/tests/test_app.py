import asyncio
from time import sleep

from fastapi.testclient import TestClient

from ..application import Application
from ..api import AppStatus
from ..app import app


Application.configure('test')
asyncio.run(Application.build())
app.state.limiter.enabled = False
client = TestClient(app)


def test_status():
    response = client.get('/status/')
    assert response.status_code == 200
    response_json = response.json()
    response_data = AppStatus(**response_json)
    assert 'de' in response_data.sourceLanguages
    assert 'zh' in response_data.sourceLanguages
    assert 'de' in response_data.targetLanguages
    assert 'zh' in response_data.targetLanguages


def test_translate():
    request_data = {
        'sourceLanguage': 'zh',
        'targetLanguage': 'fr',
        'sourceText': '生活就像一盒巧克力。'
    }
    response = client.post('/translate/', json=request_data)
    assert response.status_code == 200
    response_json = response.json()
    assert 'requestId' in response_json
    assert response_json['status'] == 'QUEUED'


def test_translate_limit():
    try:
        app.state.limiter.enabled = True
        request_data = {
            'sourceLanguage': 'zh',
            'targetLanguage': 'fr',
            'sourceText': '生活就像一盒巧克力。'
        }
        # Test that the service can only be called once in 20 seconds.
        response = client.post('/translate/', json=request_data)
        assert response.status_code == 200
        response = client.post('/translate/', json=request_data)
        assert response.status_code == 429
        sleep(20)
        response = client.post('/translate/', json=request_data)
        assert response.status_code == 200
    finally:
        app.state.limiter.enabled = False
    