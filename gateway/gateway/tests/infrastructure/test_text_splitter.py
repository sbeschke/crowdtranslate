from gateway.infrastructure.text_splitter import TextSplitter

def test_text_splitter_should_split_two_sentences_if_they_do_not_hit_the_limit():
    splitter = TextSplitter(max_characters=100)

    chunks = [
        "Dieser Satz hat dreißig Zeichen.",
        "Dieser Satz hat auch dreißig Zeichen."
    ]
    assert splitter.split(" ".join(chunks)) == chunks


def test_text_splitter_should_split_two_sentences_if_they_hit_the_limit():
    splitter = TextSplitter(max_characters=50)

    chunks = [
        "Dieser Satz hat dreißig Zeichen.",
        "Dieser Satz hat auch dreißig Zeichen."
    ]
    assert splitter.split(" ".join(chunks)) == chunks


def test_text_splitter_should_split_one_sentence_on_the_token_boundary():
    splitter = TextSplitter(max_characters=20)

    chunks = [
        "Dieser Satz hat",
        "beinahe vierzig",
        "Zeichen.",
    ]
    assert splitter.split(" ".join(chunks)) == chunks


def test_text_splitter_should_split_a_long_token():
    splitter = TextSplitter(max_characters=10)

    chunks = [
        "langeslang", "eslangesla", "nges"
    ]
    assert splitter.split("".join(chunks)) == chunks
