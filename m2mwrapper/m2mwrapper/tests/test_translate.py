from ..translate import TranslationService

def test_translation_service():
    service = TranslationService()

    # translate Hindi to French
    hi_text = "जीवन एक चॉकलेट बॉक्स की तरह है।"
    hi_trans = service.translate(hi_text, 'hi', 'fr')
    assert hi_trans == 'La vie est comme une boîte de chocolat.'

    # translate Chinese to English
    chinese_text = "生活就像一盒巧克力。"
    chinese_trans = service.translate(chinese_text, 'zh', 'en')
    assert chinese_trans == 'Life is like a box of chocolate.'
