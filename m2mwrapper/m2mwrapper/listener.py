#!/usr/bin/env python
import sys

import pika
import typer

from crowdtranslate_messaging import messaging

from .translate import TranslationService


def main(hostname: str = 'localhost'):
    service = TranslationService()

    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=hostname))
    channel = connection.channel()

    channel.exchange_declare(exchange='translation_requests', exchange_type='topic')
    channel.exchange_declare(exchange='translation_responses', exchange_type='fanout')

    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue

    channel.queue_bind(
        exchange='translation_requests', queue=queue_name, routing_key='translation.#')
    
    print(' [*] Waiting for translation requests. To exit press CTRL+C')


    def callback(ch, method, properties, body):
        print(f' [x] {method.routing_key}: {body}')
        request = messaging.TranslationRequest.parse_raw(body, content_type='application/json')
        target_text = service.translate(request.source_text, request.source_language, request.target_language)
        response = messaging.TranslationResponse(request_id=request.request_id, target_text=target_text)
        channel.basic_publish(
            exchange='translation_responses', routing_key='', body=response.json())


    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)

    channel.start_consuming()



if __name__ == "__main__":
    typer.run(main)
