from transformers import M2M100ForConditionalGeneration, M2M100Tokenizer


class TranslationService:
    def __init__(self):
        self.model = M2M100ForConditionalGeneration.from_pretrained("facebook/m2m100_1.2B")
        self.tokenizer = M2M100Tokenizer.from_pretrained("facebook/m2m100_1.2B")

    def translate(self, source_text: str, source_language: str, target_language: str):
        self.tokenizer.src_lang = source_language
        encoded = self.tokenizer(source_text, return_tensors='pt')
        generated = self.model.generate(**encoded, forced_bos_token_id=self.tokenizer.get_lang_id(target_language))
        decoded = self.tokenizer.batch_decode(generated, skip_special_tokens=True)
        return decoded[0]