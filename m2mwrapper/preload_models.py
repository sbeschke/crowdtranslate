from transformers import M2M100ForConditionalGeneration, M2M100Tokenizer

def preload_models():
    model = M2M100ForConditionalGeneration.from_pretrained("facebook/m2m100_1.2B")
    tokenizer = M2M100Tokenizer.from_pretrained("facebook/m2m100_1.2B")


if __name__ == '__main__':
    preload_models()